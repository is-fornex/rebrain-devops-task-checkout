# Chatting-dict

A chatting interface with built-in dictionary for laravel projects.

## Requirements
------------
 - PHP >= 7.0.0
 - Laravel >= 5.5.0
 - Fileinfo PHP Extension

### Installation
------------
> This package requires PHP 7+ and Laravel 6.0, for old versions please refer to [1.4](#)

First, install laravel 6.0, and make sure that the database connection settings are correct.

```
composer require test/chatting-dict
```

Then run these commands to publish assets and config：

```
php artisan vendor:publish --provider="Test\Chatting\ChattingServiceProvider"
```

### License
------------
`chatting-dict` is licensed under [The MIT License (MIT)](LICENSE)
